# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def insertIntoBST(self, root, val):
        """
        :type root: TreeNode
        :type val: int
        :rtype: TreeNode
        """
        if root is None:
            root = TreeNode(val)
        def findLeaf(root, val):
            if root is None: 
                root = TreeNode(val)
                return root
            if root.val>val: 
                root.left = findLeaf(root.left,val)
            else: root.right= findLeaf(root.right,val)
            return root
        
        return findLeaf(root,val)
