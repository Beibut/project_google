class Solution:
    def repeatedNTimes(self, A):
        """
        :type A: List[int]
        :rtype: int
        """
        # my solution
        d={}
        for a in A:
            if a in d: return a
            d[a]=1
        return -1
    
        #OFFICIAL solution using HASHMAP
        count = collections.Counter(A)
        for k in count:
            if count[k] > 1:
                return k