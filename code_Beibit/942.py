class Solution(object):
    def diStringMatch(self, S):
        """
        :type S: str
        :rtype: List[int]
        """
        curr = 0
        res = []
        minn, maxx = 0, len(S)
        if S[0] == 'D': 
            curr = maxx
            maxx-=1
        else: minn+=1

        res.append(curr)
        for l in S[1:]:
            if l == 'I':
                res.append(minn)
                minn+=1
            else:
                res.append(maxx)
                maxx-=1
        res.append(minn)
        return res
