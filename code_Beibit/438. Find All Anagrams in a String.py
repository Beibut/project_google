class Solution:
    def findAnagrams(self, s: 'str', p: 'str') -> 'List[int]':
        res = []
        if len(s) < len(p): return res
        curr = [0]*26
        pDict = [0]*26
        lenP = len(p)
        for letter in p:
            pDict[ord(letter)-ord('a')] += 1
        for i in range(len(s)):
            if i>=lenP:
                curr[ord(s[i-lenP])-ord('a')]-=1
            curr[ord(s[i])-ord('a')]+=1
            if curr == pDict: res.append(i-lenP+1)
        
        return res

# using Counter
class Solution2:
    def findAnagrams(self, s: 'str', p: 'str') -> 'List[int]':
        from collections import Counter
        res = []
        if len(s) < len(p): return res
        pDict = Counter(p)
        lenP = len(p)
        curr = Counter(s[:lenP])
        if pDict == curr: res.append(0)
        for i in range(lenP, len(s)):
            curr[s[i-lenP]]-=1
            if curr[s[i-lenP]] == 0: del curr[s[i-lenP]]
            curr[s[i]]+=1
            if curr == pDict: res.append(i-lenP+1)
        
        return res