class Solution:
    def uniquePathsIII(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        self.res =0

        def findGrid(grid,n):
            if len(grid)==0: return (-1,-1)
            for i in range(len(grid)):
                for j in range(len(grid[0])):
                    if grid[i][j] == n: return (i,j)
            return (-1,-1)

        def dfs(grid, el):
            i,j= el[0], el[1]
            if i>=len(grid) or i<0 or (len(grid)>0 and j>=len(grid[0])) or j<0: return 
            if grid[i][j] == -1: return
            if grid[i][j] == 2: 
                for row in grid:
                    for el in row: 
                        if el == 0: return 
                self.res+=1
                return
            if grid[i][j] == 3: return
            grid[i][j] = 3
            dfs(grid, (i+1, j))
            dfs(grid, (i-1, j))
            dfs(grid, (i, j+1))
            dfs(grid, (i, j-1))
            grid[i][j] = 0
            return

        beg = findGrid(grid,1)
        if beg == (-1,-1): return 0
        dfs(grid, beg)
        return self.res

        