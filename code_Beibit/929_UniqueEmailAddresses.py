class Solution:
    def numUniqueEmails(self, emails):
        """
        :type emails: List[str]
        :rtype: int
        """
        uniqueEmails = set()
        for email in emails:
            local, domain = email.split('@')
            plus = local.find('+')
            if plus !=-1: local = local[:plus]
            uniqueEmails.add(local.replace('.','')
                     +email[email.find('@'):])

        return len(uniqueEmails)

