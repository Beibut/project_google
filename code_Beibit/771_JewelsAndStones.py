class Solution:
    def numJewelsInStones(self, J, S):
        """
        :type J: str
        :type S: str
        :rtype: int
        """
        stones = {}
        for s in S:
            if s in stones: stones[s]+=1
            else: stones[s]=1
        res=0
        for j in J:
            if j in stones: res+=stones[j]
        return res

# One liner solutions from Stefan Pochmann
    def numJewelsInStones1(self, J, S):
        # sum - sum of list
        # map - map function-J.count to input-S
        # J.count(S) - number of occurrences of S in J
        return sum(map(J.count, S))
    def numJewelsInStones2(self, J, S):
        return sum(map(S.count, J))    
    def numJewelsInStones3(self, J, S):
        return sum(s in J for s in S)

print(Solution().numJewelsInStones('aA','aaAABBS'))