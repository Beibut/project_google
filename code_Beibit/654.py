# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def constructMaximumBinaryTree(self, nums):
        """
        :type nums: List[int]
        :rtype: TreeNode
        """
        if len(nums) == 0: return None
        maxx =max(nums)
        indexMax = nums.index(maxx)
        root = TreeNode(maxx)
        root.left = self.constructMaximumBinaryTree(nums[:indexMax])
        root.right = self.constructMaximumBinaryTree(nums[indexMax+1:])
        
        return root
        