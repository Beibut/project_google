class Codec:

    def __init__(self):
        self.encoder = {}
        self.decoder = {}

    def encode(self, longUrl):
        """Encodes a URL to a shortened URL.
        
        :type longUrl: str
        :rtype: str
        """
        if longUrl in self.encoder: return self.encoder[longUrl]
        import string
        import random
        def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
            return ''.join(random.choice(chars) for _ in range(size))

        shortUrl = id_generator()
        while shortUrl in self.decoder:
            shortUrl = id_generator()
        self.decoder[shortUrl] = longUrl
        self.encoder[longUrl] = shortUrl
        return shortUrl
        
    def decode(self, shortUrl):
        """Decodes a shortened URL to its original URL.
        
        :type shortUrl: str
        :rtype: str
        """
        if shortUrl in self.decoder: return self.decoder[shortUrl]
        return shortUrl
        

# Your Codec object will be instantiated and called as such:
# codec = Codec()
# codec.decode(codec.encode(url))