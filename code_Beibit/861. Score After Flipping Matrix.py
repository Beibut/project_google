class Solution:
    def matrixScore(self, A):
        """
        :type A: List[List[int]]
        :rtype: int
        """
        def flip(s):
            return ''.join('0' if e=='1' else '1' for e in s)
        
        for index,row in enumerate(A):
            num = ''.join(str(e) for e in row)
            curr = int(num,2)
            flipped = int(flip(num),2)
            if flipped > curr:
                A[index] = [0 if a == 1 else 1 for a in A[index]] 
        print(A)
        for j in range(len(A[0])):
            one = 0
            for i in range(len(A)):
                one+= 1 if A[i][j] else -1
            if one < 0:
                for i in range(len(A)):
                    A[i][j] = int(not A[i][j])

        res=0
        for a in A:
            res += int(''.join(str(e) for e in a),2)

        return res

# GREADY solution