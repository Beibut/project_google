class Solution(object):
    def sortArrayByParity(self, A):
        """
        :type A: List[int]
        :rtype: List[int]
        """
        def mod2(x): return x%2
        return sorted(A, key=mod2)