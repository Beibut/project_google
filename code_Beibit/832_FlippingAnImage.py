class Solution(object):
    def flipAndInvertImage(self, A):
        """
        :type A: List[List[int]]
        :rtype: List[List[int]]
        """        
        def invert(x): return 0 if x==1 else 1 
        return [map(invert, reversed(row)) for row in A]
