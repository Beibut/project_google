class Solution:
    def findAndReplacePattern(self, words, pattern):
        """
        :type words: List[str]
        :type pattern: str
        :rtype: List[str]
        """
        res = []
        pDict = {}
        i=1
        pLst = []
        for p in pattern: 
            if p not in pDict: 
                pDict[p] = i; i+=1
            pLst.append(pDict[p])

        for word in words:
            pDict = {}
            i = 1
            wLst = []
            for w in word:
                if w not in pDict: 
                    pDict[w] = i; i+=1
                wLst.append(pDict[w])
                if pLst[:len(wLst)] != wLst: break

            if pLst == wLst: res.append(word)
        
        return res