class Solution:
    def sortedSquares(self, A):
        """
        :type A: List[int]
        :rtype: List[int]
        """
        # default solution
        return sorted(map(lambda x: x ** 2, A))
        
        # second solution: abs->sort->sqr
        A = sorted(map(abs, A))
        return list(map(lambda x: x**2, A))
