class Solution:
    def allPathsSourceTarget(self, graph):
        """
        :type graph: List[List[int]]
        :rtype: List[List[int]]
        """
        self.res =[]
        N = len(graph)
        seen = [False]*N
        
        def dfs(v,curr):
            if v == N-1:
                curr.append(v)
                self.res.append(curr.copy())
                curr.pop()
                return 
            seen[v] = True
            curr.append(v)
            for neighbor in graph[v]:
                if not seen[neighbor]:
                    dfs(neighbor,curr)
            seen[v] = False
            curr.pop()
            
            return
        dfs(0,[])
        return self.res