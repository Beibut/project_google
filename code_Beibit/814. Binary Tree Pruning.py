# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def pruneTree(self, root):
        """
        :type root: TreeNode
        :rtype: TreeNode
        """
        def rec(root):
            if root is None: return 0
            sleft = rec(root.left)
            sright = rec(root.right)
            if sleft == 0: root.left = None
            if sright == 0: root.right = None
            return sleft+sright+ root.val
            
        s = rec(root)
        if s == 0: return None
        
        return root
        
        