# https://www.youtube.com/watch?v=RR4SoktDQAw
# looking at the processes in the OS process manager 

import os
import time
from multiprocessing import current_process, Process

def square(nums):
    for num in nums:
        # delay of 0.5 sec
        time.sleep(0.5)
        print(f"The square of {num} equals  {num*num}")

if __name__ == '__main__':
    lst = range(100)
    processes = []
    for i in range(50):
        process = Process(target=square, args=(lst,))
        processes.append(process)
        process.start()
    
    for process in processes:
        process.join()
    print("Multiprocessing finished")