# https://www.youtube.com/watch?v=RR4SoktDQAw
# Multiprocessing squaring function and accessing current processes to check 

import os
from multiprocessing import current_process, Process

def square(num):
    # Gets the id of the current process
    process_id = os.getpid()
    print(f"Process id: {process_id}")

    # another way to get current process
    process_name = current_process().name
    print(f"Process name: {process_name}")

    print(f"The square of {num} equals  {num*num}")

if __name__ == '__main__':
    lst = [1,2,3,4]
    processes = []
    for num in lst:
        process = Process(target=square, args=(num,))
        processes.append(process)
        process.start()