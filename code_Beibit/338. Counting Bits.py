class Solution:
    def countBits1(self, num: 'int') -> 'List[int]':
        return [bin(n).count('1') for n in range(num+1)]
    
    def countBits(self, num: 'int') -> 'List[int]':
        res = [0]
        while len(res) < num+1:
            tmp = [x+1 for x in res]
            res.extend(tmp)

        return res[:num+1]

# Derived second solution after checking result for differnt values