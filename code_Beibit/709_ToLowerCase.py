class Solution:
    def toLowerCase(self, s):
        """
        :type str: str
        :rtype: str
        """
        # default solution
        # return str.lower()
        
        # print(chr(ord('H')+ord('a')-ord('A')))
        return "".join(chr(ord(l)+ord('a')-ord('A')) if 'A'<=l and l<='Z' else l for l in s)