class Solution:
    def complexNumberMultiply(self, a: 'str', b: 'str') -> 'str':
        a1, a2 = a.split('+')
        a1=int(a1)
        a2 = int(a2[:-1])
        b1, b2 = b.split('+')
        b1=int(b1)
        b2 = int(b2[:-1])
        
        resA = a1*b1 - a2*b2
        resB = a1*b2 + a2*b1
        
        res = f'{resA}+{resB}i'
        return res