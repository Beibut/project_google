class Solution:
    def minAddToMakeValid(self, S):
        """
        :type S: str
        :rtype: int
        """
        res = 0
        curr = 0
        for s in S:
            if s == '(': curr+=1
            else: curr-=1
            if curr<0:
                curr = 0
                res+=1
        
        res+=curr
        return res