class Solution(object):
    def judgeCircle(self, moves):
        """
        :type moves: str
        :rtype: bool
        """
        v = 0
        h = 0
        for m in moves:
            if m == 'U': v+=1
            elif m == 'D': v-=1
            elif m == 'L': h-=1
            elif m == 'R': h+=1
        return True if not v and not h else False