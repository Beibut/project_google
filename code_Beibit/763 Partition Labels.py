class Solution:
    def partitionLabels(self, S):
        """
        :type S: str
        :rtype: List[int]
        """
        # find rthe first and last occurence of each letter
        maxx = len(S) +1
        lettersMin = [maxx]*26
        lettersMax = [-1]*26
        for index, val in enumerate(S):
            l = ord(val) - ord('a')
            lettersMin[l] = min(index, lettersMin[l])
            lettersMax[l] = max(index, lettersMax[l])

        res = []
        curr = 0
        prev = 0
        for index, val in enumerate(S):
            l = ord(val) - ord('a')
            if lettersMin[l] == index: curr+=1
            if lettersMax[l] == index: curr-=1

            if curr == 0: 
                res.append(index+1 - prev)
                prev= index+1

        return res

class Solution2(object):
    def partitionLabels(self, S):
        last = {c: i for i, c in enumerate(S)}
        j = anchor = 0
        ans = []
        for i, c in enumerate(S):
            j = max(j, last[c])
            if i == j:
                ans.append(i - anchor + 1)
                anchor = i + 1
            
        return ans