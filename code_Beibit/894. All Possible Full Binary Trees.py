# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def allPossibleFBT(self, N):
        """
        :type N: int
        :rtype: List[TreeNode]
        """
        if N == 0 : return []
        root = TreeNode(0)
        if N == 1: return [root]
        prev = [None, root]
        curr = []
        for i in range(2,N+1):
            for j in range(len(prev)):
                newRoot = TreeNode(0)
                newRoot.left = prev[i]
                newRoot.right = prev[len(prev)-i-1]
                curr.append(newRoot)
        
        return curr