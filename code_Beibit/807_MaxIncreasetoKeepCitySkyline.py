class Solution:
    def maxIncreaseKeepingSkyline(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        if len(grid) == 0: return 0
        totalAdd = 0
        n, m = len(grid), len(grid[0])
        rows,cols = [0]*n, [0]*m
        for i in range(n):
            rows[i] = max(grid[i])
        for j in range(m): 
            for i in range(n):
                cols[j] = max(cols[j], grid[i][j])

        for i in range(n):
            for j in range(m):
                totalAdd += min(rows[i], cols[j]) - grid[i][j]
        return totalAdd

    # Shortened solution
        def maxIncreaseKeepingSkyline(self, grid):
            row_maxes = [max(row) for row in grid]
            col_maxes = [max(col) for col in zip(*grid)]

            return sum(min(i, j) for i in row_maxes for j in col_maxes) - sum(map(sum, grid))
                    

print (Solution().maxIncreaseKeepingSkyline([[3,0,8,4],[2,4,5,7],[9,2,6,3],[0,3,1,0]]))