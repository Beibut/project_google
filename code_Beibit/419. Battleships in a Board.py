class Solution:
    def countBattleships(self, board):
        """
        :type board: List[List[str]]
        :rtype: int
        """
        res = 0
        for i in range(len(board)):
            for j in range(len(board[i])):
                if board[i][j] == 'X':
                    if ((board[i][j-1]=='.' and j-1>=0) or j-1<0) and ((board[i-1][j]=='.' and i-1>=0) or i-1<0):
                        res+=1
                        
        return res