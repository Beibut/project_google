# solve like for parentheses. When start +1, when end -1 and find max while looping through
# O(n) time, O(n) additional space
class MyCalendarThree(object):

    def __init__(self):
        self.bookings = {}
        self.points = set()
        
    def book(self, start, end):
        """
        :type start: int
        :type end: int
        :rtype: int
        """
        self.points.add(start)
        self.points.add(end)
        if start in self.bookings:
            self.bookings[start]+=1
        else: self.bookings[start] = 1
        if end in self.bookings:
            self.bookings[end]-=1
        else: self.bookings[end] = -1
        res = 0
        current =0
        for num in sorted(self.points):
            current +=self.bookings[num]
            res = max(res, current)
        return res
    
# Your MyCalendarThree object will be instantiated and called as such:
# obj = MyCalendarThree()
# param_1 = obj.book(start,end)