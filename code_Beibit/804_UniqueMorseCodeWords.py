class Solution:
    def uniqueMorseRepresentations(self, words):
        """
        :type words: List[str]
        :rtype: int
        """
        self.letters = [".-","-...","-.-.","-..",".","..-.","--.","....","..",
                   ".---","-.-",".-..","--","-.","---",".--.","--.-",".-.",
                   "...","-","..-","...-",".--","-..-","-.--","--.."]
        def transformed(word):
            return "".join(self.letters[ord(w)-ord('a')] for w in word)
    
        unique = set()
        for word in words:
            unique.add(transformed(word))
        return len(unique)
        
